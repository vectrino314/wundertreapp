import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import { EvaIconsPack } from '@ui-kitten/eva-icons';
import {
  mapping,
  light as lightTheme,
  dark as darkTheme,
} from '@eva-design/eva';
import {
  ApplicationProvider,
  IconRegistry,
  Layout,
  Text,
  Button,
} from 'react-native-ui-kitten';
import { LinkButton } from '../components/LinkButton';
import { FacebookButton, GoogleButton } from '../components/SocialBtns';

export const ThemedButtonShowcase = props => <LinkButton {...props} />;

const ApplicationContent = () => (
  <Layout style={styles.container}>
    <SafeAreaView>
      <View style={styles.container}>
        <View style={styles.imagePlaceholder}></View>
        {/* Sign In Button */}
        {/* Sign Up Button */}
        <View style={styles.buttonGroup}>
          <FacebookButton></FacebookButton>
          <GoogleButton></GoogleButton>
        </View>
      </View>
    </SafeAreaView>
  </Layout>
);

const Login: React.FC = () => {
  type toggleTheme = 'lightTheme' | 'darkTheme';

  return (
    <React.Fragment>
      <IconRegistry icons={EvaIconsPack} />
      <ApplicationProvider mapping={mapping} theme={darkTheme}>
        <ApplicationContent />
      </ApplicationProvider>
    </React.Fragment>
  );
};

const styles = StyleSheet.create({
  container: {
    borderWidth: 4,
    borderColor: Colors.white,
    color: Colors.red,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imagePlaceholder: {
    width: 150,
    height: 150,
    borderRadius: 150 / 2,
    backgroundColor: Colors.white,
    marginBottom: 30,
  },
  buttonGroup: {
    borderWidth: 4,
    borderColor: Colors.white,
    flexDirection: 'column',
    justifyContent: 'space-between',
    height: 100,
    width: 'auto',
  },
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default Login;
