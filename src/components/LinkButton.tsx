import React from 'react';
import { Button, ButtonProps, withStyles } from 'react-native-ui-kitten';

type ButtonElement = React.ReactElement<ButtonProps>;

export const BasicButton = (props?: ButtonProps): ButtonElement => {
  const { styles, themedStyle } = this.prop;

  return (
    <Button style={[styles, themedStyle]} status="basic" {...props}>
      BUTTON
    </Button>
  );
};

export const LinkButton = withStyles(Button, theme => ({
  backgroundColor: theme['color-primary-default'],
}));
