import React from 'react';
import { Button, Icon } from 'react-native-ui-kitten';

export const FacebookIcon = (style) => (
  <Icon name='facebook' {...style} />
);

export const GoogleIcon = (style) => (
  <Icon name='google' {...style} />
);

export const FacebookButton = () => (
  <Button icon={FacebookIcon}>Login with Facebook</Button>
);

export const GoogleButton = () => (
  <Button icon={GoogleIcon}>Login with Google</Button>
);